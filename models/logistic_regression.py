import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics

import sys
import warnings


class LogisticReg:
    def __init__(self, dataset):
        self.dataset = dataset

    def logistic_reg(self):
        df_x, df_y = self.split()

        reg = LogisticRegression()
        x_train, x_test, y_train, y_test = train_test_split(df_x, df_y, test_size=0.33, random_state=42)
        reg.fit(x_train, y_train)

        y_pred = reg.predict(x_test)

        response = reg.predict([[0, 0, 0, 0, 0, 0]])
        self.results(response, y_pred, y_test)

    def split(self):
        dataset = self.dataset
        df_x = pd.DataFrame(self.dataset)
        df_x = pd.DataFrame(
            np.c_[
                dataset['question1'], dataset['question2'],
                dataset['question3'], dataset['question4'],
                dataset['question5'], dataset['question6']
            ],
            columns=[
                'question1', 'question2',
                'question3', 'question4',
                'question5', 'question6',
            ]
        )

        df_y = pd.DataFrame(dataset.divorced)

        return df_x, df_y

    def results(self, response, pred, test):
        for res in response:
            print("Segun los valores de la fila usted se " +
                  ("se divorciara" if res == 1 else "conservara su matrimonio"))

        cnf_matrix = metrics.confusion_matrix(test, pred)
        print(cnf_matrix)

        print('Accuracy', metrics.accuracy_score(test, pred))
