import pandas as pd

import sys
import warnings

from analysis import Analysis
from models.logistic_regression import LogisticReg


def main():
    if sys.warnoptions:
        warnings.simplefilter('ignore')

    dataset = pd.read_csv('data/divorce.csv')
    divorced = pd.DataFrame(dataset)

    df = pd.DataFrame()
    df['question1'] = divorced['Atr2'].values
    df['question2'] = divorced['Atr6'].values
    df['question3'] = divorced['Atr11'].values
    df['question4'] = divorced['Atr18'].values
    df['question5'] = divorced['Atr26'].values
    df['question6'] = divorced['Atr40'].values
    df['divorced'] = divorced['Class'].values

    print(df.describe())
    print(df.info)

    an = Analysis(dataset=df)
    an.graphs()

    lr = LogisticReg(dataset=df)
    lr.logistic_reg()


if __name__ == '__main__':
    main()
