import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn import metrics

import sys
import warnings

if sys.warnoptions:
    warnings.simplefilter('ignore')

dataset = pd.read_csv('data/divorce.csv')

divorced_form = pd.DataFrame(dataset)

df = pd.DataFrame()
df['question1'] = divorced_form['Atr2'].values
df['question2'] = divorced_form['Atr6'].values
df['question3'] = divorced_form['Atr11'].values
df['question4'] = divorced_form['Atr18'].values
df['question5'] = divorced_form['Atr26'].values
df['question6'] = divorced_form['Atr40'].values
df['divorced'] = divorced_form['Class'].values

df_x = pd.DataFrame(df)
df_x = pd.DataFrame(
    np.c_[
        df['question1'],
        df['question2'],
        df['question3'],
        df['question4'],
        df['question5'],
        df['question6']
    ],
    columns=[
        'question1',
        'question2',
        'question3',
        'question4',
        'question5',
        'question6',
    ]
)

df_y = pd.DataFrame(df.divorced)

reg = LogisticRegression()

x_train, x_test, y_train, y_test = train_test_split(df_x, df_y, test_size=0.33, random_state=42)
reg.fit(x_train, y_train)

y_pred = reg.predict(x_test)
array = df.values

response = reg.predict([[0, 0, 0, 0, 0, 0]])
print(len(response))

cnf_matrix = metrics.confusion_matrix(y_test, y_pred)
print(cnf_matrix)
print("Accuracy:", metrics.accuracy_score(y_test, y_pred))
print(reg.get_params(True))
