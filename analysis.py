import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


class Analysis:
    def __init__(self, dataset):
        self.dataset = dataset

    def graphs(self):
        dataset = self.dataset
        dataset.hist(figsize=(12, 9))
        plt.show()

        dataset.plot(kind='density', subplots=True, layout=(6, 6), sharex=False, legend=True, fontsize=1,
                     figsize=(12, 9))
        plt.show()

        corrmat = dataset.corr()
        f, ax = plt.subplots(figsize=(12, 9))
        k = 8
        cols = corrmat.nlargest(k, 'divorced')['divorced'].index
        cm = np.corrcoef(dataset[cols].values.T)
        sns.set(font_scale=1.25)
        hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10},
                         yticklabels=cols.values, xticklabels=cols.values)
        plt.show()
